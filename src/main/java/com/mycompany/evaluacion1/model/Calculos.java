/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.evaluacion1.model;

/**
 *
 * @author paula
 */
public class Calculos {



    private int capital;
    private float tasaInteres;
    private int anios;
    private float calculo;
    

    public Calculos(String capital, String tasaInteres, String anios) {
        this.capital = Integer.parseInt(capital);
        this.tasaInteres =Float.parseFloat(tasaInteres);
        this.anios = Integer.parseInt(anios);
    }

    
    public float getCalculo() {
        
        this.calculo = this.getCapital() * (this.getTasaInteres() / 100) * this.getAnios();
        return calculo;
    }

    
    /*
     * @return the capital
     */
    public int getCapital() {
        return capital;
    }

    /**
     * @param capital the capital to set
     */
    public void setCapital(int capital) {
        this.capital = capital;
    }

    /**
     * @return the tasaInteres
     */
    public float getTasaInteres() {
        return tasaInteres;
    }

    /**
     * @param tasaInteres the tasaInteres to set
     */
    public void setTasaInteres(int tasaInteres) {
        this.tasaInteres = tasaInteres;
    }

    /**
     * @return the anios
     */
    public int getAnios() {
        return anios;
    }

    /**
     * @param anios the anios to set
     */
    public void setAnios(int anios) {
        this.anios = anios;
    }

    

    
    
}
