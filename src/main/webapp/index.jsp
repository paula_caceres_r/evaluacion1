<%-- 
    Document   : index
    Created on : 20-09-2021, 17:32:06
    Author     : paula
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        
    </head>
    <body>
        <h1>Calculadora de interés simple</h1>
        <h1>Ingrese la información solicitada</h1>
        <form id="frmCalculadora"action="CalculadoraController" method="POST">
            <div class="form-group">
                <label for="capital">Capital:</label>
                <input type="number" name="capital" min="0" max="1000000000" class="form-control" id="capital">
            </div>
            <div class="form-group">
                <label for="tasaInteres">i (Tasa de interés anual):</label>
                <input type="number" min="0" max="100" name="tasaInteres" class="form-control" id="tasaInteres">
            </div>
            <div class="form-group">
                <label for="anios">n (Número de años):</label>
                <input type="number" min="0" max="100" name="anios" class="form-control" id="anios">
            </div>
            <button type="submit" class="btn btn-default">Calcular</button>
        </form>
    </body>
</html>
