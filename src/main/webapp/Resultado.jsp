<%-- 
    Document   : Resultado
    Created on : 21-09-2021, 23:12:57
    Author     : paula
--%>

<%@page import="com.mycompany.evaluacion1.model.Calculos"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<% 
    Calculos calc=(Calculos)request.getAttribute("calculos");

%>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>El resultado es : 
            
            <%= calc.getCalculo() %>
        
        </h1>
    </body>
</html>
